Rails.application.routes.draw do
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html

  
  resources :static, only: [] do 
    get :home
  end

  root 'static#home'
  
  resources :accounts, only: [:create] do
    collection do
      post :revoke
      post :refresh
      get :sign_in
      get :sign_up
      delete :sign_out
    end
  end

  resources :users, only: [:create, :update, :show] do
    collection do
      get :check_email
      get :edit_password
      post :reset_password
      get :forget_password
      get :dashboard
      get :me
      post '/me/change_password', to: 'users#change_password'
      get '/me/widgets', to: 'users#widgets'
    end
  end

  resources :widgets
end
