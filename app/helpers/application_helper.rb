module ApplicationHelper
  def custom_flash
    flash_messages = []
    flash.each do |type, message|
      type = 'success' if type == 'notice'
      type = 'error'   if type == 'alert'
      text = <<-JS
              <script> 
                toastr['#{type}']("#{message}");
              </script>
             JS
      flash_messages << text.html_safe if message
    end
    flash.discard
    flash_messages.join("\n").html_safe
  end
end
