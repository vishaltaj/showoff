class Users < Resource
  config do
    self.version = 'v1'
    self.path = 'users'
  end

  class << self
    def me query: {}, headers: {}
      get("#{base_uri}/api/#{version}/users/me", headers: headers)
    end

    def check_email query: {}, headers: {}
      get("#{base_uri}/api/#{version}/users/email", query: default_params.merge(query), headers: headers)
    end

    def reset_password query: {}, headers: {}
      post("#{base_uri}/api/#{version}/users/reset_password", body: default_params.merge(query.to_h).to_json, headers: default_headers(headers))
    end

    def change_password query: {}, headers: {}
      post("#{base_uri}/api/#{version}/users/me/password", body: default_params.merge(query.to_h).to_json, headers: default_headers(headers))
    end

    def update_me query: {}, headers: {}
      endpoint = "#{base_uri}/api/#{version}/#{path}/me"
      @response = put(endpoint, body: default_params(query).to_json, headers: default_headers(headers))
    end

    def widgets query: {}, headers: {}
      get("#{base_uri}/api/#{version}/users/me/widgets", query: default_params.merge(query), headers: default_headers(headers))
    end
  end
end