class Authentications < Resource
  config do
    self.path = 'oauth'
  end

  class << self
    def create(query: {}, headers: {})
      query = query.merge({grant_type: "password"})
      post("#{base_uri}/oauth/token", body: default_params.merge(query).to_json, headers: default_headers)
    end

    def refresh(query: {}, headers: {})
      post("#{base_uri}/oauth/token", body: {
            grant_type: "refresh_token",
          }.merge(query).to_json, headers: default_headers.merge(headers))
    end
  end
end