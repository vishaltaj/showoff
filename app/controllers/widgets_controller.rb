class WidgetsController < ApplicationController
  before_action :set_auth_header
  before_action :set_kind, only: [:create, :update]

  def create
    if params[:id].present?
      update_widgets
    else
      add_widgets
    end
  end

  def new
  end

  def show
    @response = Widgets.find(query: {widget: widget_params}, headers: @header) 
  end

  def index
    @response = Widgets.find(:all, query: {term: params[:term]}, headers: @header, parsed: false)
    if @response.code == 200
      @widgets = @response.parsed_response["data"]["widgets"]
    else
      @widgets = []
    end
  end

  def update
    @response = Widgets.update(query: {widget: widget_params}, headers: @header)  
  end

  def destroy
    @response = Widgets.destroy(query: {id: params[:id]}, headers: @header)
    if @response.code == 200
      redirect_to widgets_path, flash: {notice: 'Deleted Successfully'}
    else
      redirect_to widgets_path, flash: {alert: 'Something Went wrong'}
    end
  end

  private 

  def add_widgets
    @response = Widgets.create(query: {widget: widget_params}, headers: @header)
    @widget = @response.parsed_response
    if @response.code == 200
      redirect_to widgets_path, flash: {notice: 'Created Widget Successfully'}
    else
      redirect_to new_widget_path, flash: {alert: @widget["message"]}
    end
  end

  def update_widgets
    @response = Widgets.update(query: {id: params[:id], widget: widget_params}, headers: @header)  
    @widget = @response.parsed_response
    if @response.code == 200
      redirect_to widgets_path, flash: {notice: 'Updated Widget Successfully'}
    else
      redirect_to widgets_path, flash: {alert: @widget["message"]}
    end
  end

  def set_kind
    params[:widget][:kind] = params[:widget][:kind].present? ? 'visible' : 'hidden' 
  end

  def widget_params
    params.require(:widget).permit(:id, :name, :description, :kind)
  end
end