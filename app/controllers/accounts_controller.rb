class AccountsController < ApplicationController
  def create
    account = Authentications.create(query: session_params)
    @response = account.parsed_response
    if account.code == 200
      save_user_session
      redirect_to dashboard_users_path
    else
      redirect_to sign_in_accounts_path, flash: {error: @response["messasge"]}
    end
  end

  def sign_out
    if current_user.present?
      clear_sessions
      redirect_to sign_in_accounts_path
    else
      redirect_back dashboard_users_path, flash: {notice: 'Something went wrong'}
    end
  end

  def revoke
  end

  def refresh
  end

  private

  def session_params
    params.require(:account).permit(:username, :password)
  end

end