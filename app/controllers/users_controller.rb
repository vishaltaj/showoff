class UsersController < ApplicationController
  before_action :set_auth_header, except: [:create, :reset_password, :check_email]
  before_action :get_user, only: [:dashboard, :me]

  def create
    @users = Users.create(query: auth_params)
    @response = @users.parsed_response
    if @users.code == 200
      save_user_session
      redirect_to sign_in_accounts_path
    else
      redirect_to sign_up_accounts_path, flash: {alert: @response["message"]}
    end
  end

  def dashboard
  end

  def show
    
  end

  def widgets
    query = params[:term].present? ? {term: params[:term]} : {}
    @response = Users.widgets(query: query, headers: @header)
    if @response.code == 200
      @widgets = @response.parsed_response["data"]["widgets"]
    else
      @widgets = []
    end
  end

  def me
  end

  def update
    params[:user][:date_of_birth] = Time.parse(params[:user][:date_of_birth]).to_i if params[:user][:date_of_birth].present?
    @response = Users.update_me query: {user: user_params}, headers: @header
    @user = @response.parsed_response
    if @response.code == 200
      @response = @response.parsed_response
      save_user_details
      redirect_to me_users_path, flash: {notice: 'Successfully updated'}
    else
      redirect_to me_users_path, flash: {alert: 'Something went wrong'}
    end
  end

  def change_password
    @response = Users.change_password query: {user: password_params}, headers: @header
    if @response.code == 200 
      @response = @response.parsed_response
      clear_sessions
      redirect_to sign_in_accounts_path, flash: {notice: 'Successfully updated password'}
    else
      redirect_to edit_password_users_path, flash: {alert: 'Password mismatch'}
    end
  end

  def check_email
    response = Users.check_email(query: {email: params[:email]})
    render json: {}, status: response.code
  end

  def reset_password
    @response = Users.reset_password(query: {user: reset_params})
    if @response.code == 200
      redirect_to sign_in_accounts_path, flash: {notice: @response.parsed_response["message"]}
    else
      redirect_to forget_password_users_path, flash: {alert: @response.parsed_response["message"]}
    end
  end

  private

  def get_user
    if current_user.present?
      @user = current_user
    else
      @header ||= {}
      @user = Users.me(headers: @header)
      if @user.code == 200
        @response = @user.parsed_response
        @user = @response["data"]["user"]
        save_user_details
      else
        redirect_to sign_in_accounts_path
      end 
    end
  end

  def user_params
    params.require(:user).permit(:first_name, :last_name, :password, :image_url, :date_of_birth)
  end

  def auth_params
    params.require(:account).permit(user: [:first_name, :last_name, :password, :email])
  end

  def reset_params
    params.require(:user).permit(:email)
  end

  def password_params
    params.require(:user).permit(:new_password, :current_password)
  end
end