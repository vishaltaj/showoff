class ApplicationController < ActionController::Base
  helper_method :current_user, :signed_in?

  def signed_in?
    return true if cookies.signed[:user].present?
  end

  def current_user
    @current_user = cookies.signed[:user]
  end

  def save_user_session
    session[:user_token] = @response["data"]["token"]["access_token"]
    session[:refresh_token] = @response["data"]["token"]["refresh_token"]
    save_user_details if @response["data"].try(:[], "user").present?
  end

  def save_user_details
    cookies.signed[:user] = {value: @response["data"].try(:[], "user"), expires: 8.hours.from_now}
  end

  def clear_sessions
    session[:user_token] = session[:refresh_token] = cookies.signed[:user] = nil
  end

  def set_auth_header
    @header ||= {}
    @header.merge!({"Authorization" => "Bearer #{session[:user_token]}"})
  end
end
