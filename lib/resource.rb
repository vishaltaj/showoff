class Resource
  MissingParam = Class.new(StandardError)
  include HTTParty
  cattr_accessor :path, :version

  base_uri "#{Settings.api_base_url}"

  class << self
    def config(&block)
      yield
    end

    def default_params(params={})
      {
        client_id: Settings.client_id,
	      client_secret: Settings.client_secret
      }.merge(params)
    end

    def default_headers(header_params = {})
      {
        "Content-Type" => "application/json"
      }.merge(header_params)
    end

    def find(scope = :one, query: {}, headers: {}, parsed: true)
      endpoint = scope == :one ? "#{base_uri}/api/#{version}/#{path}/#{query[:id]}" : "#{base_uri}/api/#{version}/#{path}/"
      @response = get(endpoint, query: default_params(query), headers: default_headers(headers))
      @response = @response.parsed_response if parsed
      @response
    end

    def update(query: {}, headers: {})
      endpoint = "#{base_uri}/api/#{version}/#{path}/#{query[:id]}"
      @response = put(endpoint, body: default_params(query).to_json, headers: default_headers(headers))
    end

    def create(query: {}, headers: {})
      endpoint = "#{base_uri}/api/#{version}/#{path}/"
      @response = post(endpoint, body: default_params(query).to_json, headers: default_headers(headers))
    end

    def destroy(query: {}, headers: {})
      endpoint = "#{base_uri}/api/#{@@version}/#{@@path}/#{query[:id]}"
      @response = delete(endpoint, body: default_params(query).to_json, headers: default_headers(headers))
    end
  end
end